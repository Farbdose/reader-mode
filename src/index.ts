// ==UserScript==
// @name        Reader Mode Auto
// @author      Farbdose
// @description Extract pure text content from website. Focusing on webnovels.
// @version     8.8
// @namespace   *
// @include     *
// @run-at      document-start
// @grant       GM.setClipboard
// @downloadURL https://farbdose.gitlab.io/reader-mode/ReaderMode.user.js
// @require     https://farbdose.gitlab.io/reader-mode/Readability.js
// @require     https://farbdose.gitlab.io/reader-mode/jquery.min.js
// @exclude     http://localhost*
// @exclude     https://*.google.com/*
// @exclude     https://www.headspace.com/*
// @exclude     https://www.youtube.com/*
// ==/UserScript==


declare let Readability: any;

interface JQuery {
    removeAttributes(exclude?): any;
}

interface HTMLElement {
    createTextRange(): any;
}

interface Array<T> {
    toRegex: any;
    last: any;
}

class ReaderMode {

    private text: string = "";
    private linkNext: any;
    private linkPrev: any;
    private hasRun: boolean = false;
    private titleParts: Array<any>;
    private allTitleParts: Array<any>;
    private titlePartsInContent: Array<any>;
    private site_meta: any;

    private include: Array<any> = [{
        "regex": [
            "https?://(www.)?royalroadl.com/fiction/[0-9]+/.+/chapter/[0-9]+/.+"
        ],
        "author": [],
        "story": [],
        "website": [
            "royalroadl"
        ]
    }, {
        "regex": [
            "https?://arkmachinetranslations.com/chapter-*"
        ],
        "author": [
            "Rainbow Turtle"
        ],
        "story": [
            "Ark"
        ],
        "website": [
            "ark machinetranslations"
        ]
    }, {
        "regex": [
            "https?://requirecookie.com/series/[a-z0-9\-]+/[a-z0-9\-]+/"
        ],
        "author": [
            "Stormy"
        ],
        "story": [
            "Mirrorfall",
            "Mirrorheart",
            "Mirrorshades",
            "Finding A Signal"
        ],
        "website": [
            "Require: Cookie"
        ]
    }, {
        "regex": [
            "https?://gravitytales.com/[Nn]ovel/[a-zA-Z0-9_-]+/.*-chapter-.*"
        ],
        "author": [
            "mooderino",
            "Daman"
        ],
        "story": [
            "How To Avoid Death On A Daily Basis",
            "The Good Student",
            "The Divine Elements"
        ],
        "website": [
            "gravity tales"
        ]
    }, {
        "regex": [
            "https?://.*\.(com|org|net)/.*chapter.*",
            "https?://thegam3.com/20*/*/*/*/*",
            "https?://www.baka-tsuki.org/project/index.php?title=*",
            "https?://skythewood.blogspot.de/*/*/*.html",
            "https?://*.wordpress.com/*/*/*/*/",
            "https?://thesylthorian.com/*/*/*/*/",
            "https?://.*.wordpress.com/dddd/dd/.*$",
            "https?://www.fictionpress.com/s/2961893/*/*",
            "https?://leonardpetracci.com/20*/*/*/*/",
            "https?://www.starwalkerblog.com/*/*",
            "https?://www.reddit.com/r/HFY/comments/*/oc_humans_dont_make_good_pets*",
            "https?://www.reddit.com/r/HFY/comments/*/*jenkinsverse_*",
            "https?://www.reddit.com/r/HFY/comments/*/oc_kevin_jenkins_*",
            "https?://www.ironteethserial.com/dark-fantasy-story/online-story/*/",
            "https?://www.ironteethserial.com/dark-fantasy-story/*/"
        ],
        "story": [],
        "author": ["PuDDleS4263"],
        "website": ["on Patreon", "Patreon"]
    }];

    private exclude: Array<any> = [{
        "regex": [
            "https?://www.starwalkerblog.com/category/*"
        ],
        "author": [],
        "website": [],
        "story": []
    }];

    constructor() {
        let self = this;

        jQuery["fn"].removeAttributes = function (exclude?) {
            exclude = (exclude === undefined) ? [] : exclude;
            return this.each(function () {
                let attributes = $.map(this.attributes, function (item) {
                    return item.name;
                });
                let e = $(this);
                $.each(attributes, function (i, item) {
                    if (exclude.indexOf(item) == -1) {
                        e.removeAttr(item);
                    }
                });
            });
        };

        let jQexpr = <any>jQuery.expr[":"];
        jQexpr.regex = (elem: any, index, match) => {
            let matchParams = match[3].split(','),
                validLabels = /^(data|css):/,
                attr = {
                    method: matchParams[0].match(validLabels) ?
                        matchParams[0].split(':')[0] : 'attr',
                    property: matchParams.shift().replace(validLabels, '')
                },
                regexFlags = 'ig',
                regex = new RegExp(matchParams.join('').replace(/^\s+|\s+$/g, ''), regexFlags);
            return regex.test((<any>jQuery(elem)[attr.method])(attr.property));
        };

        jQexpr.parentF = function (elem, index, match) {
            return jQuery(elem).parent(match[3]).length < 1;
        };

        jQexpr.htmlContains = function (elem, index, match) {
            return new RegExp(match[3], 'ig').test(elem.innerHTML);
        };

        window["jq"] = jQuery;

        Array.prototype["toRegex"] = function () {
            return this.map(function (s) {
                if (s.match(/([\(\)\[\]\*\+\{\}\^\$]|(\\n|\\b|\\w|\\d))/)) {
                    return s.replace(/(^|[^\.\]\)])\*/g, "$1\.\*");
                } else {
                    return s.replace(/(\.|\[|\]|,)/g, "\\$1");
                }
            });
        };

        if (!Array.prototype["last"]) {
            Array.prototype["last"] = function () {
                return this[this.length - 1];
            };
        }

        Readability.prototype._getArticleMetadata = function () {
            let metadata: any = {};
            let values: any = {};
            let metaElements = this._doc.getElementsByTagName("meta");

            // Match "description", or Twitter's "twitter:description" (Cards)
            // in name attribute.
            let namePattern = /^\s*((twitter)\s*:\s*)?(description|title)\s*$/gi;

            // Match Facebook's Open Graph title & description properties.
            let propertyPattern = /^\s*og\s*:\s*(description|title)\s*$/gi;

            // Find description tags.
            this._forEachNode(metaElements, function (element) {
                let elementName = element.getAttribute("name");
                let elementProperty = element.getAttribute("property");

                if ([elementName, elementProperty].some(function (e) {
                    return /author|creator/i.test(e);
                })) {
                    metadata.byline = element.getAttribute("content");
                    return;
                }

                let name = null;
                if (namePattern.test(elementName)) {
                    name = elementName;
                } else if (propertyPattern.test(elementProperty)) {
                    name = elementProperty;
                }

                if (name) {
                    let content = element.getAttribute("content");
                    if (content) {
                        // Convert to lowercase and remove any whitespace
                        // so we can match below.
                        name = name.toLowerCase().replace(/\s/g, '');
                        values[name] = content.trim();
                    }
                }
            });

            if ("description" in values) {
                metadata.excerpt = values["description"];
            } else if ("og:description" in values) {
                // Use facebook open graph description.
                metadata.excerpt = values["og:description"];
            } else if ("twitter:description" in values) {
                // Use twitter cards description.
                metadata.excerpt = values["twitter:description"];
            }

            let possible_titles = [];
            if (!metadata.title) {
                if ("og:title" in values) {
                    // Use facebook open graph title.
                    possible_titles.push(values["og:title"]);
                }
                if ("twitter:title" in values) {
                    // Use twitter cards title.
                    possible_titles.push(values["twitter:title"]);
                }
            }

            metadata.title = this._getArticleTitle(possible_titles);
            return metadata;
        };

        /**
         * Get the article title as an H1.
         *
         * @return void
         **/
        Readability.prototype._getArticleTitle = (function (possible_titles) {
            console.log("possible titles: ", possible_titles);
            let doc = this._doc;
            let body = document.body;
            let html = document.documentElement;
            let docHeight = Math.max(body.scrollHeight, body.offsetHeight,
                html.clientHeight, html.scrollHeight, html.offsetHeight);
            let yOffset = (window.scrollY || window.pageYOffset);
            let curTitle = "";
            let origTitle = "";

            try {
                // fine more title candidates
                /*let ptitle = $("h1:contains('Chapter'), h2:contains('Chapter'), h3:contains('Chapter'), h4:contains('Chapter')")
                 .add("div:regex(class,(entry|post)\-?title)")
                 .add("h1:regex(class,(entry|post)\-?title)")
                 .add("h2:regex(class,(entry|post)\-?title)")
                 .add("h1")
                 .add("h2")
                 .filter(":not(:contains('Next Chapter'))")
                 .filter(":not(:contains('Previous Chapter'))");*/

                self.titleParts = [];
                let selectors = [
                    "h1",
                    "h2",
                    "div:regex(class,(entry|post)\-?title)",
                    "h1:regex(class,(entry|post)\-?title)",
                    "h2:regex(class,(entry|post)\-?title)",
                    "h3:regex(class,(entry|post)\-?title)",
                    "h1:contains('Chapter')",
                    "h2:contains('Chapter')",
                    "h3:contains('Chapter')",
                    "h4:contains('Chapter')"
                ];

                selectors.forEach((selector, i) => {
                    self.titleParts = self.titleParts.concat($(selector).map((_, e) => {
                        return {
                            "element": e,
                            "selector": selector,
                            "text": $(e).text(),
                            "score": {
                                "tag": i,
                                /* normalize top relative to document height */
                                "top": (e.getBoundingClientRect().top + yOffset) / docHeight,
                                "position": -1,
                                "total": -1
                            }
                        };
                    }).toArray());
                });

                // only consider elements in the upper 3rd of the document
                self.titleParts = self.titleParts.filter((a) => {
                    return a.score.top < 0.3;
                });

                self.titleParts.sort((a, b) => {
                    return a.score.top - b.score.top;
                });

                self.titleParts.forEach((part, i) => {
                    part.score.position = i;

                    // use top score as tie breaker betweeen tag and position
                    part.score.total = part.score.tag + part.score.position + part.score.top;
                });

                self.titleParts.sort((a, b) => {
                    return a.score.total - b.score.total;
                });

                console.log(self.titleParts);

                self.titleParts = self.titleParts.map((part) => {
                    $(part.element).remove();
                    return part.text;
                });

                self.titleParts = possible_titles.concat(self.titleParts);

                console.log("Initial article title parts: ", self.titleParts);

                let ptitle = self.titleParts.last();
                if ("undefined" !== typeof ptitle && ptitle.length > 0) {
                    curTitle = origTitle = ptitle;
                } else {
                    curTitle = origTitle = doc.title;
                    // If they had an element with id "title" in their HTML
                    if (typeof curTitle !== "string") {
                        curTitle = origTitle = this._getInnerText(doc.getElementsByTagName('title')[0]);
                    }
                }
            } catch (e) {
                console.log(e);
                /* ignore exceptions setting the title. */
            }

            curTitle = origTitle = curTitle.replace(/(^| )Vol (\d)/i, " $1Volume $2").replace(/(^| )Ch (\d)/i, "$1Chapter $2").replace(/^.*(?=Chapter)/i, "");
            //console.log("uuuh", curTitle)
            let hOnes;
            if (curTitle.match(/ [\|\-\\\/>»] /)) {
                curTitle = origTitle.replace(/(.*)[\|\-\\\/>»] .*/gi, '$1');
                if (curTitle.split(' ').length < 3) {
                    curTitle = origTitle.replace(/[^\|\-\\\/>»]*[\|\-\\\/>»](.*)/gi, '$1');
                }
            } else if (curTitle.indexOf(': ') !== -1) {
                // Check if we have an heading containing this exact string, so we
                // could assume it's the full title.
                let headings = this._concatNodeLists(
                    doc.getElementsByTagName('h1'),
                    doc.getElementsByTagName('h2')
                );
                let match = this._someNode(headings, function (heading) {
                    return heading.textContent === curTitle;
                });

                // If we don't, let's extract the title out of the original title string.
                if (!match) {
                    curTitle = origTitle.substring(origTitle.lastIndexOf(':') + 1);

                    // If the title is now too short, try the first colon instead:
                    if (curTitle.split(' ').length < 3) {
                        curTitle = origTitle.substring(origTitle.indexOf(':') + 1);
                    }
                }
            } else if (curTitle.length > 150 || curTitle.length < 15) {
                hOnes = doc.getElementsByTagName('h1');

                if (hOnes.length === 1) {
                    curTitle = this._getInnerText(hOnes[0]);
                }
            }

            hOnes = doc.getElementsByTagName('h1');
            if (hOnes.length > 1 && hOnes.length < 4) {
                //try to shorten title
                let h0text = this._getInnerText(hOnes[0]);
                if (curTitle.indexOf(h0text) != -1) {
                    let short = curTitle.replace(h0text, "");
                    if (short.length > 6 && (!short.match(/Chapter/i) || short.length >= 14)) {
                        short = short.replace(/~+([a-zA-Z]+)~+/g, "$1").replace(/\-\s*$/, "").replace(/\s+/g, " ").trim();
                        if (!short.match(/Chapter/i) && short.match(/^\d/)) {
                            short = "Chapter: " + short;
                        }
                        curTitle = origTitle = short;
                    }
                }
            }

            curTitle = curTitle.trim();
            //console.log(curTitle+"              "+origTitle)
            let curTitleLen = curTitle.split(' ').length;
            if (curTitleLen <= 4 && (!origTitle.match(/ [\\\/>»] /) ||
                curTitleLen != origTitle.replace(/[\|\-\\\/>» ]+/g, " ").split(' ').length - 1)) {
                curTitle = origTitle;
            }

            if (self.titleParts.indexOf(curTitle) == -1) {
                self.titleParts.push(curTitle);
            }

            return curTitle.replace(/\~/g, " ").trim();
        });

        //let readyStateBackup = document.onreadystatechange;
        let onreadystatechange = function () {
            console.log("Ready state changed to: " + document.readyState);
            if (document.readyState == "interactive") {
                self.init();
            }

            /*try {
                readyStateBackup();
            }catch(e){}*/
        };

        //document.onreadystatechange = onreadystatechange;
        $(document).on('readystatechange', onreadystatechange);
        console.log("Registered ready state change");

        /*$(window).load( () => {
            window.setTimeout(() => {
                self.fixFullscreen();
            }, 500);
        });*/

        $(document).on("keydown", function (e) {
            //console.log(e);
            if (e.ctrlKey) {
                if (e.keyCode == 66) {
                    self.run();
                }

                if (e.keyCode == 39) {
                    self.startAutoCrawl();
                }

                if (e.keyCode == 40) {
                    // localStorage["ReaderModeAutoCrawl"] = "false";
                    self.stopAutoCrawl();
                }
            }
        });
    }


    hashCode(str) {
        let hash = 0, i = 0, len = str.length, chr;
        while (i < len) {
            hash = ((hash << 5) - hash + str.charCodeAt(i++)) << 0;
        }
        return hash;
    }

    wordsToNumber(str) {
        const orig = str;

        const Small = {
            'zero': 0,
            'one': 1,
            'two': 2,
            'three': 3,
            'four': 4,
            'five': 5,
            'six': 6,
            'seven': 7,
            'eight': 8,
            'nine': 9,
            'ten': 10,
            'eleven': 11,
            'twelve': 12,
            'thirteen': 13,
            'fourteen': 14,
            'fifteen': 15,
            'sixteen': 16,
            'seventeen': 17,
            'eighteen': 18,
            'nineteen': 19,
            'twenty': 20,
            'thirty': 30,
            'forty': 40,
            'fifty': 50,
            'sixty': 60,
            'seventy': 70,
            'eighty': 80,
            'ninety': 90
        };

        const Magnitude = {
            'thousand': 1000,
            'million': 1000000,
            'billion': 1000000000,
            'trillion': 1000000000000,
            'quadrillion': 1000000000000000,
            'quintillion': 1000000000000000000,
            'sextillion': 1000000000000000000000,
            'septillion': 1000000000000000000000000,
            'octillion': 1000000000000000000000000000,
            'nonillion': 1000000000000000000000000000000,
            'decillion': 1000000000000000000000000000000000
        };

        let res = "";
        let n = 0, g = 0, hits = 0;
        const searchReg = /(\s*num\s*)+/;
        str.toString().replace(/-+g/, " #DASH# ").split(/\s+/).forEach((word) => {
            let key = word.toLowerCase().replace(/[^a-zA-Z0-9]/, "");
            let x = Small[key];
            if (x != null) {
                g = g + x;
                res += word.replace(new RegExp(key, "i"), " num");
            } else if (word == "hundred") {
                g = g * 100;
                res += word.replace(new RegExp(key, "i"), " num");
            } else {
                x = Magnitude[word];
                if (x != null) {
                    n = n + g * x;
                    g = 0;
                    res += word.replace(new RegExp(key, "i"), " num");
                } else {
                    if (searchReg.test(res)) {
                        res = res.replace(/(\s*num\s*)+/, " " + (n + g) + " ");
                        hits += 1;
                    }
                    res += " " + word;
                    n = g = 0;
                }
            }
        });

        res = res.replace(/(\s*num\s*)+/, " " + (n + g) + " ");
        res = res.replace(" #DASH# ", "-").trim();

        console.log("wordsToNumbers result", res);
        if (hits > 2) {
            res = orig;
        }

        return res;
    }

    autoCrawlCallback(text) {
        console.log("copying to clipboard...");
        GM.setClipboard(this.text);
    }

    stopAutoCrawl() {
        console.log("Checking AutoCrawl...");
        if (this.isCrawl()) {
            console.log("AutoCrawl Finished...");
            localStorage["ReaderModeAutoCrawl"] = "false";
            localStorage["forceReaderModeOnce"] = "false";
            this.text = localStorage['ReaderModeStory'];
            localStorage['ReaderModeStory'] = "";
            localStorage['ReaderModeHashes'] = "";

            $("head").first().html("<ReaderModeOn></ReaderModeOn>");
            $("html").add("body").removeAttributes();
            $("body").html("<div style='padding: 50px' id='box'><pre>" + this.text + "</pre></div>");
            this.selectText($("body")[0]);
            console.log("ok");
            this.autoCrawlCallback(this.text);
            try {
                window.stop();
            } catch (e) {
            }
        }
    }

    startAutoCrawl() {
        localStorage["ReaderModeAutoCrawl"] = "true";
        console.log("Starting AutoCrawl...");
        if (this.hasRun) {
            this.next();
        } else {
            localStorage['ReaderModeStory'] = "";
            localStorage['ReaderModeHashes'] = "";
            this.run();
        }
    }

    isCrawl() {
        return localStorage["ReaderModeAutoCrawl"] == "true";
    }

    next() {
        let link = this.linkNext;
        console.log("Trying to load next page...");
        if (typeof link === 'undefined') {
            console.log("Error no link!");
            this.stopAutoCrawl();
            return;
        }

        let timeout = window.setTimeout(() => {
            console.log("Redirect failed!");
            localStorage["forceReaderModeOnce"] = "false";
            this.stopAutoCrawl();
        }, 5000);

        window.onbeforeunload = (e) => {
            window.clearTimeout(timeout);
        };

        localStorage["forceReaderModeOnce"] = "true";

        console.log("Trying to redirect with: ", link);
        if (typeof link === "string" && /^http/.test(link)) {
            window.location.href = link;
        } else if (link.hasAttribute("href") && /^http/.test($(link).attr("href"))) {
            window.location.href = $(link).attr("href");
        } else {
            link.click();
        }
    }

    prev() {
        let link = this.linkPrev;

        console.log("Trying to load prev page...");
        if (typeof link === 'undefined') {
            console.warn("Error no link! Redicrecting using browser history...");
            window.history.back();
            return;
        }

        localStorage["forceReaderModeOnce"] = "true";

        console.log("Trying to redirect with: ", link);
        if (typeof link === "string" && /^http/.test(link)) {
            window.location.href = link;
        } else if (link.hasAttribute("href") && /^http/.test($(link).attr("href"))) {
            window.location.href = $(link).attr("href");
        } else {
            link.click();
        }
    }

    selectText(container) {
        window.getSelection().removeAllRanges();
        let range = document.createRange();
        range.selectNode(container);
        window.getSelection().addRange(range);
    }

    getSelectedText() {
        return window.getSelection().toString();
    }

    onlyUnique(value, index, se) {
        return se.lastIndexOf(value) === index;
    }

    sanitize(arr) {
        let san = (str) => str
            .replace(/\s+/g, " ")
            .replace(/﻿/, "")
            .replace(/[–—]/g, "-")
            .replace(/[′’‘]/g, "'")
            .replace(/[^a-zA-Z0-9\-\:\!\.\,\;\'\"]/g, " ")
            .replace(/\s+/g, " ")
            .replace(/\bVolume\b/i, "Volume")
            .replace(/\bChapter\b/i, "Chapter")
            .replace(/\bAct\b/i, "Act")
            .replace(/^[\-\:\!\.\,\;\'\" ]+|[\-\:\!\.\,\;\'\" ]+$/, "")
            .trim();

        if (typeof arr === 'string') {
            return san(arr);
        } else if ($.isArray(arr)) {
            return arr.map(san).filter(this.onlyUnique);
        }
    }

    replacePageName(arr) {
        if (typeof arr === 'string') {
            if (typeof this.site_meta === "undefined") {
                let pageName = document.location.hostname.replace(/(\w+\.)*(\w+)\.\w+/, "$2");
                for (let i = 0; i < pageName.length + 1; i++) {
                    let partA = pageName.substr(0, i);
                    let partB = pageName.substr(i);
                    let pattern = new RegExp(partA + (i == 0 ? "" : " ") + partB, "i");
                    //console.log("Testing pattern: ", pattern);
                    if (pattern.test(arr)) {
                        return arr.replace(pattern, "").trim();
                    }
                }
            } else {
                //console.log(this.site_meta, this.site_meta.story,this.site_meta.author,this.site_meta.website);
                let patterns = this.site_meta.story.concat(this.site_meta.author).concat(this.site_meta.website);
                for (let i = 0; i < patterns.length; i++) {
                    //console.log(arr, patterns[i]);
                    arr = arr.replace(new RegExp("\W*" + patterns[i] + "\W*"), " ").trim();
                }
            }
            return arr;
        } else if ($.isArray(arr)) {
            return arr.map((e) => {
                return this.replacePageName(e);
            }).filter(this.onlyUnique).filter((str) => {
                return str != "";
            });
        }
    }

    removeInvis() {
        let keepToken = "k33p4r";
        console.time("removeInvis: by token");
        const windowHeight = (window.innerHeight || document.documentElement.clientHeight);
        const windowWidth = (window.innerWidth || document.documentElement.clientWidth);

        $("body").find("*").each((_, ele) => {
            let e = $(ele);
            let rect = ele.getBoundingClientRect();

            const horInView = (rect.left <= windowWidth) && ((rect.left + rect.width) >= 0);
            if (!horInView) {
                return;
            }

            if (e.is("br,hr,p")) {
                e.parentsUntil("[" + keepToken + "]")
                    .add(e)
                    .attr(keepToken, "");
                return;
            } else if (e.is(":hidden")) {
                return;
            }

            // delete to small elements
            if (rect.height * rect.width > 16 || e.text().length > 1000) {
                e.parentsUntil("[" + keepToken + "]")
                    .add(e)
                    .attr(keepToken, "");
            }
        });
        $("head, title, meta").attr(keepToken, "");
        $(":not([" + keepToken + "]").remove();
        $("*").removeAttr(keepToken);

        console.timeEnd("removeInvis: by token");
    }

    replaceNbsps(str) {
        let re = new RegExp(String.fromCharCode(160), "g");
        return str.replace(re, " ");
    }

    getInContentTitleParts(element, maxDistance = 100, selector = "h1, h2, h3, h4, strong, b, u") {
        let curLen = 0, candidates = [];

        // https://stackoverflow.com/a/50664277/2422125
        function _findEls(el) {
            $(el).contents().each(function () {
                if (curLen < maxDistance) {
                    if ($(this).is(selector) || ($(this).is("p:contains(Chapter):not(:contains(Next Chapter), :contains(Previous Chapter), :contains(Last Chapter))"))) {
                        let t = $(this).text();
                        // catch elements that are actually breaklinese before the content begins (TODO may delete break after potential author's note)
                        if (candidates.length == 0 && !/[a-zA-Z0-9]/.test(t)) {
                            return this.remove();
                        } else if (!/[\[\]\*]/.test(t) && !/\bPOV\b|\bpoint of view\b/i.test(t)) {
                            candidates.push(this);
                        }
                    } else if (this.firstElementChild /* it has children */) {
                        /* only count text of element OR its children */
                        return _findEls(this);
                    }

                    curLen += $(this).text().length;
                }
            });
        }

        _findEls(element);

        console.log("Candidates Raw: ", candidates, candidates[0]);

        // remove strong, b, u elements if they selected more than 2 elements
        candidates = $(candidates).not(["strong", "b", "u"].filter((tag) => {
            return $(candidates).filter(tag).length > 2;
        }).join(",")).toArray();

        console.log("Candidates Filtered: ", candidates);

        return $(candidates);
    }

    noBR(i, e) {
        return $(e).text().trim().split("\n").length == 1;
    }

    extractLinks(possibleLinks: Array<any>, blacklist: Array<RegExp>) {
        let res, link;
        for (let z = 0; z < possibleLinks.length && typeof res === 'undefined'; z++) {
            res = possibleLinks[z];
            if (res && typeof res !== "string" && res.length) {
                res = res[0];
            }

            try {
                if (!res.length || res.length > 0) {
                    res = $(res);

                    // console.log("pre enabled check:", res.toArray());
                    res = res.filter(function () {
                        return !$(this).is(":disabled");
                    });
                    // console.log("post enabled check:", res.toArray());
                    res = res.filter(function () {
                        //temp hotfix
                        return true;
                        /*if (/https?:\/\//.test($(this).attr("href"))){
                            let ans =  window.location.href.split("/").replace(/\/$/,"").length == $(this).attr("href").replace(/\/$/,"").split("/").length ;
                            if(!ans){
                                console.log(window.location.href.replace(/\/$/,"").split("/").length, $(this).attr("href").replace(/\/$/,"").split("/").length)
                                console.log(window.location.href.replace(/\/$/,""), $(this).attr("href").replace(/\/$/,""))
                            }
                            return ans
                        }else if(/\//.test($(this).attr("href"))){
                            return window.location.href.split("/").length-2 == $(this).attr("href").split("/").length;
                        }else if(/@/.test($(this).attr("href"))){
                            return false;
                        }else{
                            return true;
                        }*/
                    })[0];

                    if (res) {
                        console.log("Found Links: ", res, "with query number: ", z);
                        if ($(res).text().length < 200) {
                            const wrapped = $(res).wrap("<p>").html();
                            if (!blacklist.some(regex => regex.test(wrapped))) {
                                // pass true to clone() to copy event handlers
                                link = $(res).clone(true)[0];
                            }

                            try {
                                $(res).remove();
                                console.log("removed...");
                            } catch (e) {
                            }
                        } else {
                            console.warn("not removed as content is to big...");
                        }
                    }
                }
            } catch (e) {
                console.warn("Error executing query number: ", z, "msg: ", e);
            }
        }

        return link;
    }

    run() {
        if (this.hasRun) {
            console.log("Aleady in ReaderMode, exiting");
            return;
        } else {
            this.hasRun = true;
        }
        try {
            $(document).on("keydown", (e) => {
                if (e.keyCode == 37) {
                    this.prev();
                } else if (e.keyCode == 39) {
                    this.next();
                }
            });
            
            // this may display errors in the console, just ignore them, the script will continiue
            window.stop();

            console.time("Stop timers/intervals by bruteforce");
            // top everything else
            for (let i = 1, l = setTimeout(() => {
            }); i <= l; i++) {
                clearTimeout(i);
            }
            for (let i = 1, l = setInterval(() => {
            }); i <= l; i++) {
                clearInterval(i);
            }
            console.timeEnd("Stop timers/intervals by bruteforce");

            //remove links to external domains
            let domain_begin: any = window.location.href.split("/");
            domain_begin = domain_begin[0] + "//" + domain_begin[2];
            console.log("Deleting links leading away from: ", domain_begin);
            $("a[href^='http']:not(a[href^='" + domain_begin + "'])").remove();

            //deleting Footer, experimental TODO, test!!
            let footer = $("footer");
            if (footer.length > 0) {
                console.log("Removed experimental footer: ", footer);
                footer.remove();
            }

            let maxEntries = (r, n) => {
                return r && r.length && r.length < n ? r.last() : undefined;
            };

            this.linkNext = this.extractLinks([
                $("a:contains('ext Chapter')").last(),
                $("button:contains('ext Chapter')").last(),
                $("a:contains('Next')").last(),
                $("button:contains('Next')").last(),
                maxEntries($("a").clone().wrap("<p>").parent().filter(":htmlContains(Next)").children(), 3),
                maxEntries($("div:regex(class,.*navigation.*|.*navlinks.*)").last().find("a"), 3),
                maxEntries($("a").clone().wrap("<p>").parent().filter(":htmlContains(Chapter)").children().filter(function () {
                    return !$(this).parent().add($(this).parent().parent()).has(":htmlContains(Recent)");
                }), 3),
                $("div:contains('ext Chapter')").last(),
                $("div:contains('Next')").last(),
                $("nav").find("a[rel='next']")
            ], [/Previous/]);

            this.linkPrev = this.extractLinks([
                $("a:contains('rev Chapter')").last(),
                $("button:contains('rev Chapter')").last(),
                $("a:contains('Prev')").last(),
                $("button:contains('Prev')").last(),
                maxEntries($("a").clone().wrap("<p>").parent().filter(":htmlContains(Prev)").children(), 3),
                $("div:contains('rev Chapter')").last(),
                $("div:contains('Prev')").last(),
                $("nav").find("a[rel='prev']")
            ], [/Next/]);

            // console.log($("a"))
            if (this.linkPrev) {
                console.log("Choose Prev link: " + $(this.linkPrev).text().trim() + " with url: " + this.linkPrev);
            } else {
                console.warn("No link to prev chapter found!");
            }

            if (this.linkNext) {
                console.log("Choose Next link: " + $(this.linkNext).text().trim() + " with url: " + this.linkNext);
            } else {
                console.warn("No link to next chapter found!");
            }

            //return

            // treat images as breaklines
            $("img").replaceWith("<p>##############</p>");

            $("a").filter((_, e) => /Next|Previous|Last( Chapter)?|Index|Table of Contents/.test($(e).text())).remove();

            [
                ["Id", "#"],
                ["Class", "."]
            ].forEach(function (el) {
                Array.prototype["makeReg" + el[0]] = function () {
                    return this.map(function (s) {
                        if (s.match(/([\(\)\[\]\*\+\{\}\^\$]|(\\n|\\b|\\w|\\d))/)) {
                            return ":regex(" + el[0].toLowerCase() + "," + s + ")";
                        } else {
                            return el[1] + s;
                        }
                    });
                };
            });

            let sharedRegex = [
                //'*shar[e|ed|ing]*',
                '*reaction*',
                'sidebar',
                '*menu*',
                '*dropdown*',
                '*choice*',
                'user',
                '*footer*',
                '.*comment(?!s?-page).*',
                '*respond*',
                '*announcement*',
                '*banner*',
                '*btn*',
                '*button*',
                '*search*',
                '*control*',
                '*navigation*',
                '*nav*',
                '*settings*',
                '*actions*',
                '*icon*',
                '*image*',
                '*img*',
                // '*info*',
                //'*edit*',
                '*tools*',
                '*login*',
                '*logout*',
                '*mail*',
                '*date*',
                '*subscription*',
                '*reference*',
                '*gallery*',
                '*support*',
                '*contact*',
                '*modal*',
                '*alert*'
            ];


            let delId = [
                'jp-post-flair',
                'branding',
                'secondary',
                'colophon',
                'cookie-law-info-bar',
                'masthead',
                'secondary',
                'categories',
                'jumpToContent'
            ].concat(sharedRegex).toRegex();

            let delClass = [
                //'caption',
                // 'profile',
                'profile-info',
                'scroll-to-top',
                'background-overlay',
                'cc[_\-]*',
                'score',
                'domain',
                'trow1',
                'googlepublisherpluginad',
                'ningbar',
                'navi',
                'ads',
                'smalltext',
                'breadcrumb',
                'su-tabs-nav',
                'su-list',
                'credits',
                'noprint',
                'toc',
                'thumb',
                'sow-cta-base',
                'jp-relatedposts',
                'tagline',
                'fa-paypal',
                'socicon-patreon',
                'modal-backdrop',
                'commentarea',
                'copyright-obfuscation'
            ].concat(sharedRegex).toRegex();

            let baseTags = ["HTML", "HEAD", "BODY"];

            delId = new RegExp("^(" + delId.join("|") + ")$");
            delClass = new RegExp("^(" + delClass.join("|") + ")$");


            console.time("Big remove: collect selectors");
            let all = [].slice.call(document.getElementsByTagName("*"));
            let classes = all.map((a) => {
                return a.getAttribute("class");
            }).join(" ").split(" ").filter(this.onlyUnique).filter((e) => delClass.test(e));
            let ids = all.map((a) => {
                return a.getAttribute("id");
            }).filter(this.onlyUnique).filter((e) => delId.test(e));
            console.timeEnd("Big remove: collect selectors");

            console.time("Big remove: collect elements");
            let toDelete = [];
            classes.forEach((e) => {
                let eles = document.getElementsByClassName(e);
                if (eles !== null && baseTags.indexOf(e.tagName) == -1) {
                    toDelete = toDelete.concat([].slice.call(eles));
                }
            });

            ids.forEach((e) => {
                let ele = document.getElementById(e);
                if (ele !== null && baseTags.indexOf(e.tagName) == -1) {
                    toDelete.push(ele);
                }
            });

            toDelete = toDelete.filter((e) => baseTags.indexOf(e.tagName) == -1);
            console.timeEnd("Big remove: collect elements");

            console.time("Big remove: remove elements");
            $(toDelete).filter((_, e) => !!e.firstElementChild).remove();
            $(toDelete).remove();
            console.timeEnd("Big remove: remove elements");

            this.removeInvis();

            $("h5").add("h6").remove();

            /* fix insane usage of spans in p 
               the tag list in the below if is basically a html tag whitelist 
               regarding the content of p elements, every other formatting is removed*/
            $("p").filter((_, f) => $(f).find("p").length == 0).each((_, e) => {
                if ($(e).find("strong, b, u, a").length == 0) {
                    e.normalize();

                    // the usage of a regex requires <br> to not have any attributes or classes
                    $(e).find("br").each((__, ee) => {
                        while (ee.attributes.length > 0) {
                            ee.removeAttribute(ee.attributes[0].name);
                        }
                    });

                    // remove fake linebreaks
                    $(e).find("*:not(pre):not(code)").addBack().contents().each(function (__, ee) {
                        if (ee.nodeType == Node.TEXT_NODE) {
                            ee.nodeValue = (ee as Text).nodeValue.replace(/\n/g, " ").trim();
                        } else if (ee.nodeType == Node.ELEMENT_NODE) {
                            (ee as HTMLElement).innerHTML = (ee as HTMLElement).innerHTML.replace(/\n/g, " ").trim();
                        }
                    });

                    e.innerHTML = e.innerHTML.replace(/<br>/g, "\n");
                    e.innerHTML = this.replaceNbsps(
                        $(e).text()
                            .replace(/[\u00A0-\u9999<>\&]/gim, (i) => '&#' + i.charCodeAt(0) + ';')
                            .replace(/\r/g, "")
                            .replace(/\n/g, "<br>")
                    );
                }
            });

            // remove small ps
            $("*").filter((_, e) => $(e).text().trim().length == 0).each((_, e) => {
                $(e).html("");
            });
            $("p").filter((_, e) => $(e).text().trim().length <= 1).each((_, e) => {
                $(e).html("");
            });

            this.removeInvis();
            //if(1==1) return;


            let loc = document.location;
            let uri = {
                spec: loc.href,
                host: loc.host,
                prePath: loc.protocol + "//" + loc.host,
                scheme: loc.protocol.substr(0, loc.protocol.indexOf(":")),
                pathBase: loc.protocol + "//" + loc.host + loc.pathname.substr(0, loc.pathname.lastIndexOf("/") + 1)
            };

            $("div.reaction_buttons").remove();

            $("tr").each((_, e) => {
                let td = $(e).find("td:not(:last-child)");
                td.html(td.html() + " ");
            });

            let article = new Readability(uri, document);
            // $("body").html(article.content);
            // if(1 == 1) return;


            let content_length = $("body").text().replace(/\s/g, "").length;
            if (content_length < 2000) {
                console.log("Content to small: " + content_length + " looking for real chapter link...");
                //return
                let target_link = $("a:contains('Chapter')");
                if (0 < target_link.length && target_link.length < 3 && typeof target_link[0] !== 'undefined') {
                    //console.log("Found potential Chapter links: ", target_link);
                    console.log("Redirecting to: ", target_link[0]);
                    this.linkNext = target_link[0];
                    this.next();
                    return;
                }
                console.log("No link found! Trying to parse...");
            }

            console.time("Readability");
            article = window["article"] = article.parse();
            console.timeEnd("Readability");
            console.log(article);
            if (article == null) {
                console.log("Parsing Error: article is null");
                this.stopAutoCrawl();
                return;
            }

            let box: any = window["box"] = jQuery('<div/>', {
                html: article.content
            });

            box.find("p").filter((_, e) => $(e).text().length <= 1).each((_, e) => $(e).remove());

            let pages = box.find(":regex(id,readability-page-*)");
            pages.attr("class", "reader_mode_page");
            let pageOne = pages.first();

            //remove header and footer
            pages.find("hr").parent().find(":nth-child(20)").prevAll().filter("hr").last().prevAll().addBack().remove();
            pages.find("hr").parent().find(":nth-last-child(20)").nextAll().filter("hr").first().nextAll().addBack().remove();
            pages.find("hr").replaceWith("<p>##############</p>");

            //remove more notes from the autor
            pages.find(":nth-last-child(10)")
                .nextAll()
                .filter("p")
                .filter((_, e) => /\bwriter\b/i.test($(e).text()) && /\bIf you\b/i.test($(e).text()))
                .remove();


            console.log("lifting only-childs up...");
            pages.find("*").filter(":parentF(#box,p,h1,h2,h3,h4,strong,u,b,.reader_mode_page,table,tr,td,tbody):only-child()").filter((_, e) => {
                let t = $(e).text().trim();
                return t.length > 0 && t == $(e).parent().text().trim();
            }).each((_, e) => {
                let p = $(e).parent();
                console.log("replacing ", p[0], " with ", e);
                p.replaceWith(e);
            });


            //more cleanup
            pages.find("*").removeAttributes(["style"]);


            console.log("pre", this.titleParts);
            this.titleParts = this.sanitize(this.replacePageName(this.titleParts.map((e) => {
                return this.wordsToNumber(e);
            })));
            console.log("after", this.titleParts);

            const longestTitlePart = this.titleParts.reduce(function (a, b) {
                return a.length > b.length ? a : b;
            });
            let inContTmp = this.getInContentTitleParts(pageOne, Math.max(longestTitlePart.length * 5 + 200));
            this.titlePartsInContent = this.sanitize(inContTmp.toArray().map((e) => {
                return this.sanitize(this.wordsToNumber($(e).text()));
            }));
            console.log("TitleParts from Article:", this.titleParts);
            console.log("TitleParts from content:", this.titlePartsInContent);
            console.log(this.site_meta);
            this.allTitleParts = this.titleParts.concat(this.titlePartsInContent).filter(this.onlyUnique);
            inContTmp.remove();

            // weed out composite titles
            if (this.allTitleParts.length > 1) {
                this.allTitleParts = this.allTitleParts.filter((e) => {
                    let f = e;
                    this.allTitleParts.forEach((p) => {
                        if (p != e) {
                            f = f.replace(p, "");
                        }
                    });

                    f = f.replace(/[^a-zA-Z0-9]/g, "");
                    return f.length > 0;
                });

                console.log("After composite filter: ", this.allTitleParts);
            }

            let title = "";
            if (this.allTitleParts.length > 0) {
                title = this.allTitleParts[0];
                if (/Chapter/.test(title)) {
                    title = title.replace(/^.*(?=Chapter)/i, "");
                }
            }

            if (this.allTitleParts.length > 1) {
                let partsWithWordChapter = this.allTitleParts.filter((e) => e.match(/Chapter/i));
                if (partsWithWordChapter.length == 0) {
                    console.log("no parts contain the word chapter -> use last part");
                    title = this.allTitleParts.last();
                } else if (partsWithWordChapter.length == 1 || partsWithWordChapter.indexOf(this.allTitleParts.last()) == -1) {
                    console.log("exactly one part contains word chapter (or the text has title text at the beginning)-> use last part that contains word chapter + everything after");
                    this.allTitleParts = this.allTitleParts.slice(this.allTitleParts.indexOf(partsWithWordChapter.last()));
                    this.allTitleParts = this.allTitleParts.map((e) => e.replace(/^.*(?=Chapter)/i, ""));
                    title = this.allTitleParts.filter((e, i) => i == 0 || this.allTitleParts[0].indexOf(e) == -1).join(" ");
                } else if (partsWithWordChapter.length > 1) {
                    console.log("so there are more than one chapter parts and one of them is the last element of allTitleParts");
                    //remove parts before first chapter found
                    this.allTitleParts = this.allTitleParts.slice(this.allTitleParts.indexOf(partsWithWordChapter[0]));
                    this.allTitleParts = this.allTitleParts.map((e) => e.replace(/^.*(?=Chapter)/i, ""));
                    console.log("is there a part with more than chapter + chapter number?");
                    let the_part = null;
                    this.allTitleParts.forEach(function (e) {
                        let f = e.match(/^Chapter.{0,2}\d+.*\w{3,}/);
                        if (f) {
                            the_part = e;
                        }
                    });
                    if (the_part != null) {
                        console.log("yes: part " + the_part + " of ", this.allTitleParts);
                        console.log("chose the last one and remove everything before that one");
                        this.allTitleParts = this.allTitleParts.slice(this.allTitleParts.lastIndexOf(the_part));
                        this.allTitleParts = this.allTitleParts.map((e) => e.replace(/^.*(?=Chapter)/i, "")).filter(this.onlyUnique);
                        console.log(this.allTitleParts);
                        console.log("remove all that contain only the number");
                        let backup = this.allTitleParts;
                        this.allTitleParts = this.allTitleParts.filter((e) => !e.match(/^Chapter.{0,2}\d+$/));
                        if (this.allTitleParts.length == 0) {
                            console.log("Oh. No titles are left, restore backup...");
                            this.allTitleParts = backup;
                        }
                        console.log(this.allTitleParts);
                        console.log("use everything that is left that isn't part of the current first one");
                        title = this.allTitleParts.filter((e, i) => {
                            return i == 0 || this.allTitleParts[0].indexOf(e) == -1;
                        }).join(" ");
                        console.log("Title: " + title);
                    } else {
                        console.log("no, lets hope the chapter part at the beginning of the text is the right one");
                        title = this.allTitleParts.last();
                    }

                }
            } else {
                console.log(this.allTitleParts);
                //case 2
            }


            //begin contains chapter
            /*let chapterNode = pageOne.find(":nth-child(20)").prevAll().filter(":contains('Chapter')").first()
             if(chapterNode.length > 0){
             //put all strong text in one line
             console.log("Found Chapternode: cleaning up")
             chapterNode.html(chapterNode.html().replace(/.*(?=Chapter)/i,"")).wrap("<strong></strong>")
             fixStrong();
             }else if(article.title.match(/Chapter/i)){
             //text contains strong text at the beginning -> insert title before
             let strong = getStrong();
             console.log("Found keyword 'Chapter' at text begin")
             if(strong.length > 0){
             console.log("Text starts with strong text")
             jQuery('<strong/>', {
             //we only need chapter and the number because we already have strong text at the beginning
             text: article.title.replace(/.*(?=Chapter)/i,"").replace(/(^Chapter.*\d+).*$/i,"$1 ")
             }).insertBefore(strong.first());
             fixStrong();
             }else{
             console.log("Text starts direct")
             jQuery('<strong/>', {
             text: article.title.replace(/.*(?=Chapter)/i,"")
             }).prependTo(pageOne);
             }
             }else{
             console.log("No 'Chapter' keyword found")
             //if text starts with strong, add breaks
             let strong = getStrong();
             if(strong.length > 0){
             console.log("Text starts with strong text")
             fixStrong();
             getStrong().first().html("<strong>{{Pause=2.5}}"+getStrong().text()+"{{Pause=1.5}}</strong>");
             }else{
             console.log("Text starts direct")
             jQuery('<strong/>', {
             text: "{{Pause=2.5}}"+article.title+"{{Pause=1.5}}"
             }).prependTo(pageOne);
             }
             }*/
            // console.log(article)


            let links = box.find("a").not(":contains('.')");
            let potentially_empty = links.parent();
            links.remove();
            potentially_empty.filter((_, e) => /^\s*\|\s*\|\s*$/.test($(e).text())).remove();

            $("head").first().html("<title>" + title + "</title><ReaderModeOn></ReaderModeOn>");

            if (!title.match(/Chapter/i)) {
                if (title.match(/^\d+.*/)) {
                    title = "Chapter " + title;
                } else {
                    title = "{{Pause=2.5}}" + title + (title.match(/\w\s*$/) ? "!" : "") + "{{Pause=1.5}}";
                }
            }

            title = title.replace(/^Chapter[^\d]{0,3}(\d{1,5})(?!\d) ?[:\-,\.]?([^\r\n]{2,})$/, "Chapter $1: $2");

            box.find("h1, h2, h3, h4, h5, h6").filter((_, e) => $(e).text() == title).remove();

            if (box.text().indexOf(title) == 0) {
                console.log("FATAL ERROR: main content still starts with title!!!");
            }

            jQuery('<h2/>', {
                text: title
            }).prependTo(pageOne);

            $("html").add(box).find("*")
                .removeAttributes(["id", "class", "style"]);

            $("html").add("body").removeAttributes();

            $("body").first().html("<div style='padding: 50px' id='box'>" + box.html().replace(/(&nbsp;){5,}/g, "") + "</div>");

            box = window["box"] = $("#box")[0];

            this.selectText(box);
            this.text = this.getSelectedText();
            this.text = this.text.replace(/(^|\n) +/g, "\1");
            this.text += "\n\n";

            if (!/Author.?s? Note/i.test(title)) {
                let hash = this.hashCode(this.text);
                if (this.isCrawl()) {
                    console.log("Adding to localStorage...");
                    if (localStorage.getItem('ReaderModeStory') == null) {
                        localStorage['ReaderModeStory'] = this.text;
                        localStorage['ReaderModeHashes'] = hash;
                    } else {
                        console.log("Checking for loop...");
                        let index = localStorage['ReaderModeHashes'].split(",").indexOf(hash + "");
                        if (index != -1) {
                            console.log("Loop detected!");
                            console.log("Found hash: " + hash + " with index: " + index + " Aborting!");
                            this.stopAutoCrawl();
                            return;
                        }
                        console.log("Hash: " + hash + " is new, adding...");


                        localStorage['ReaderModeStory'] += "\n\n\n\n" + this.text;
                        localStorage['ReaderModeHashes'] += "," + hash;
                    }
                    this.next();
                } else {
                    console.log("Overwriting localStorage...");
                    localStorage['ReaderModeStory'] = this.text;
                    localStorage['ReaderModeHashes'] = hash;
                    console.log("copying to clipboard...");
                    GM.setClipboard(this.text);
                }
                return this.text;
            }

            console.log("Looks like a note form the author -> Skipping");
            if (this.isCrawl()) {
                this.next();
            }

            return "";
        } catch (e) {
            console.log(e);
        }
    }

    inIframe() {
        try {
            return window.self !== window.top;
        } catch (e) {
            return true;
        }
    }


    regexMatch(obj) {
        console.log(obj.regex);
        let el = obj.regex.map((e) => e
            .replace(/([^\/])\//g, "$1\\/")
            .replace(/(^|[^\.\]\)])\*/g, "$1\.\*")
        );
        let res = false;
        try {
            res = el.some((e) => (new RegExp(e)).test(window.location.href));
        } catch (e) {
            console.log(obj, e);
        }

        if (res) {
            console.log("site_meta", obj);
            this.site_meta = obj;
        } else {
            console.log("rejected");
        }
        return res;
    }

    checkRun() {
        let regexRes = this.include.some((obj) => this.regexMatch(obj)) && !this.exclude.some((obj) => this.regexMatch(obj));

        if (window.location.href.indexOf("#suppressReaderMode") != -1) {
            console.log("Suppressing ReaderMode");
            return false;
        }

        if (localStorage["forceReaderModeOnce"] == "true") {
            console.log("Forcing ReaderMode Once");
            localStorage["forceReaderModeOnce"] = "false";
            return true;
        }

        if (window.location.href.indexOf("#forceReaderMode") != -1) {
            console.log("Forcing ReaderMode");
            return true;
        }

        if (this.isCrawl()) {
            console.log("Crawling...");
            return true;
        }

        if (window.location.href.indexOf("#startAutoCrawl") != -1) {
            console.log("Starting AutoCrawl...");
            localStorage["ReaderModeAutoCrawl"] = "true";
            console.log("Clearing Story cache...");
            localStorage['ReaderModeStory'] = "";
        }


        return regexRes;
    }

    fixFullscreen() {
        return;
        /*if (this.exclude.some(this.regexMatch)) return;
        console.log("Fixing Fullscreen");
        let fullscreenDivs = $("div").filter(function () {
            let t = $(this);
            return t.css("position") == "fixed" && t.css("width") == $(window).width() + "px" && t.css("height") == $(window).height() + "px"
        });
        let minZindex = fullscreenDivs.css("z-index");
        fullscreenDivs.each(function () {
            minZindex = Math.min(minZindex, $(this).css("z-index"))
        });
        minZindex = Math.max(1, minZindex);

        $("div").filter(function () {
            return $(this).css("z-index") >= minZindex;
        }).remove();*/
    }

    init() {
        if (!this.inIframe()) {
            if (window.location.href == "http://royalroadl.com/my/bookmarks#forceRedirect") {
                $("a[href^='/fiction/chapter']")[0].click();
            }

            let run = this.checkRun();
            console.log("Loading TextExtractor: ", run);
            if (run) {
                this.run();
            }
        } else {
            console.log("Skipping: " + window.location.href);
        }
    }
}


function load(fromConsole) {
    if (fromConsole) {
        console.log("Starting from console, loading libraries...");
        let reader = document.createElement('script');
        reader.type = "text/javascript";
        reader.src = "https://farbdose.gitlab.io/reader-mode/Readability.js";
        document.getElementsByTagName('head')[0].appendChild(reader);

        let jq = document.createElement('script');
        jq.type = "text/javascript";
        jq.src = "https://farbdose.gitlab.io/reader-mode/jquery.min.js";
        document.getElementsByTagName('head')[0].appendChild(jq);
        window.setTimeout(function () {
            console.log("Starting ReaderMode...");
            (new ReaderMode()).run();
        }, 5000);
    } else if (window.self === window.top) {
        console.log("Running from Tampermonkey, proceeding as normal on " + window.self.location.href);
        new ReaderMode();
    }
}


load(false);





