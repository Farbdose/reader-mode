# reader-mode = mozilla/readability with specialisation on webserials 

Reader-mode is basically a custom extension of https://github.com/mozilla/readability specialised on webserial like stories that are posted chapter by chapter.

The main improvements are:

* ability to find the link to the next chapter
* automatic crawling of entirer stories
* improved extraction of chapter titles for the described usecase

# Installation

You can find the compiled userscript at https://farbdose.gitlab.io/reader-mode/ReaderMode.user.js

# Usage

The script currently supports the folling hotkeys:

* ctrl + b    start reader mode
* ctrl + →    start auto crawling (will continue until the script is unable to find the next chapter)
* pressing '→' after entering reader mode will navigate to the next chapter and display it in reader mode


Be aware that entering reader mode will automatically copy the extracted text into your clipboard.

Webserial for testing: https://www.fictionpress.com/s/2961893

